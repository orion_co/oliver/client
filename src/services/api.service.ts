import http, { AxiosRequestConfig, Method } from 'axios';

const ApiService = {
  request(resource: Method, url: string, config?: AxiosRequestConfig): Promise<any> {
    config = {
      ...config,
      method: resource,
      url,
      baseURL: 'http://localhost:3001/api',
    };

    return http.request(config)
      .catch(error => {
        throw new Error(`ApiService ${error}`);
      });
  },
};

export const BackendEventService = {
  async getAllEvents(): Promise<any> {
    const response = await ApiService.request('get', `/events`);

    return await response.data;
  },
  async getEventsQueriedByTitle(query: string|null): Promise<any> {
    const response = await ApiService.request('get', `/events/query?title=${query}`);

    return await response.data;
  },
  async getEventById(id: number): Promise<any> {
    const response = await ApiService.request('get', `/events/${id}`);

    return await response.data;
  },
  async createEvent(payload: any): Promise<any> {
    const response = await ApiService.request('post', `/events`, {data: payload});

    return await response.data;
  },
  async updateParticipationStatus(eventId: number, participantId: number, status: string): Promise<any> {
    const response = await ApiService.request(
      'put',
      `/events/${eventId}/participants/${participantId}/status`,
      {
        data: {
          status,
        },
      },
    );

    return await response.data;
  },
};

