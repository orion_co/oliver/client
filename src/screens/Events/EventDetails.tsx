import React, { useEffect, useState } from 'react';
import { Text } from 'react-native';
import { Button } from 'react-native-elements';
import { BackendEventService } from 'services/api.service';
import { LoaderComponent } from 'components/LoaderComponent';
import moment from 'moment';
import config from '@/config';
import Animated from 'react-native-reanimated';

const EventDetail = ({ route }: any): React.ReactElement => {

  const [loading, setLoading] = useState(true);
  const [event, setEvent] = useState({} as any);
  const { id } = route.params;

  useEffect(() => {
    async function fetchEvent(): Promise<void> {
      const event = await getEvent(id);
      setEvent(event);
      setLoading(false);
    }

    fetchEvent();
  }, []);

  const getEvent = async (id: number) => {
    return await BackendEventService.getEventById(id);
  };

  const handleStatusClick = async (id: number) => {
    alert(id);
  };

  return (
    <Animated.ScrollView style={{ flex: 1 }}>
      <React.Fragment>
        <LoaderComponent loading={loading} />
        {!loading &&
        <React.Fragment>
          <Text>{event.title}</Text>
          <Text>{event.description}</Text>
          <Text>{moment(event.start_date).format(config.dates.format)} - {moment(event.end_date).format(config.dates.format)}</Text>
          <Text>Total Participants: {event.participants.length}</Text>
          {event.participants?.map((p: any) => {
            return (
              <React.Fragment key={p.id}>
                <Text>
                  {p.user.name} - {p.status}
                  <Button title="Update Status" onPress={() => handleStatusClick(p.id)} />
                </Text>
              </React.Fragment>
            );
          })}
        </React.Fragment>
        }
      </React.Fragment>
    </Animated.ScrollView>
  );
};

export default EventDetail;
