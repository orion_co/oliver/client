import EventsScreen from './EventsScreen';
import EventDetail from './EventDetails';

export {
  EventsScreen,
  EventDetail,
};
