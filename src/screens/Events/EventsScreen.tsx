import React, { useCallback, useEffect, useRef, useState } from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Input } from 'react-native-elements';
import moment from 'moment';
import debounce from 'lodash.debounce';
import ActionButton from 'react-native-action-button';
import Animated from 'react-native-reanimated';
import { BackendEventService } from 'services/api.service';
import { LoaderComponent } from 'components/LoaderComponent';
import navigate from '@/navigation';
import config from '@/config';
import { Styles } from 'styles/Styles';
import { Card } from 'components/CardComponent';

const styles = StyleSheet.create({
  container: {
    margin: 0,
    padding: 0,
    marginBottom: 30,
    height: '100%',
    backgroundColor: Styles.colors.white,
  },
  item: {
    width: '100%',
  },
  title: {
    fontSize: 16,
    fontWeight: '600',
    color: Styles.colors.prpl,
  },
  button: {
    fontSize: 10,
    padding: 0,
    margin: 0,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

const EventsScreen = (): React.ReactElement => {

  const navigation = useNavigation();
  const navigator = navigate(navigation);

  const inputEl = useRef(null);
  const [events, setEvents] = useState([]);
  const [eventSearch, setEventSearch] = useState<string|null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [refresh, setRefresh] = useState<boolean>(false);

  useEffect(() => {
    async function fetchEvents(): Promise<void> {
      const events = await getEvents();
      setEvents(events);
      setLoading(false);
    }

    fetchEvents();
  }, []);

  useEffect(() => {
    setEvents([]);
    setRefresh(true);
    async function fetchEvents(): Promise<void> {
      const events = await getEventsByTitleQuery(eventSearch);
      setEvents(events);
      setRefresh(false);
    }
    fetchEvents();
  }, [eventSearch]);

  const handleRefresh = async (): Promise<void> => {
    setEvents([]);
    setRefresh(true);
    setEvents(await getEvents());
    setRefresh(false);
  };

  const getEvents = async () => {
    return await BackendEventService.getAllEvents();
  };

  const getEventsByTitleQuery = async (title: string|null) => {
    return await BackendEventService.getEventsQueriedByTitle(title);
  };

  const Item = ({ id, title, start_date, end_date, participants }: any) => (
    <Card>
      <TouchableOpacity
        key={id}
        onPress={() => navigator.openEventDetail({ id })}
      >
        <View style={styles.item}>
          <Text style={styles.title}>{title}</Text>
          <Text>{moment(start_date).format(config.dates.format)} - {moment(end_date).format(config.dates.format)}</Text>
          <Text>Participants: {participants.length}</Text>
        </View>
      </TouchableOpacity>
    </Card>
  );

  const renderEventItem = ({ item }: any) => (
    <Item {...item} />
  );

  const onEventSearch = (value: string|null) => {
    setEventSearch(value);
  };

  // Identify a better way to debounce vs. static wait in ms.
  const inputEventChangeHandler = useCallback(debounce(onEventSearch, 500), [inputEl]);

  return (
    <Animated.View style={styles.container}>
      <LoaderComponent loading={loading} />
      {!loading &&
      <React.Fragment>
        {/*  For some reason, when I put the below <Input> in a component, it re-renders each time setEventSearch state changes...  */}
        <Card
          style={{
            ...Styles.card,
            alignItems: 'center',
            marginBottom: 0,
            padding:0,
            justifyContent: 'center',
          }}>
          <Input
            ref={inputEl}
            editable={true}
            clearButtonMode={'always'}
            clearTextOnFocus={false}
            autoCapitalize='none'
            autoCorrect={false}
            onChangeText={v => inputEventChangeHandler(v)}
            placeholder='Search'
            style={{
              backgroundColor: Styles.colors.white,
            }}
          />
        </Card>
        <FlatList
          style={{
            margin: 0,
            padding: 0,
          }}
          refreshing={refresh}
          onRefresh={() => handleRefresh()}
          data={events}
          renderItem={renderEventItem}
          initialNumToRender={10}
          keyExtractor={(event: any) => event.id.toString()}
        />
      </React.Fragment>
      }
      <ActionButton onPress={() => navigator.openEventCreation()} buttonColor={Styles.colors.prpl} offsetY={20} offsetX={20} size={50} hideShadow={true}  />
    </Animated.View>
  );
};

export default EventsScreen;
