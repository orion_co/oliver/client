import React, {useState} from "react";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment/moment";
import {Input} from "react-native-elements";
import config from '@/config';
import {View} from "react-native";

interface Props {
  headerText: string,
  inputPlaceholder: string,
  onChange: (date) => any,
  mode?: 'datetime' | 'date',
  minimumDate?: null|Date,
}

const InputDateTimePicker = ({
  inputPlaceholder = 'Start Date',
  headerText = 'Select Start Date',
  mode = 'datetime',
  minimumDate,
  onChange,
}: Props): React.ReactElement => {

  const [dateValue, setDateValue] = useState(null);
  const [showPicker, setShowPicker] = useState(false);

  const handleDateChange = (date: Date): void => {
    setDateValue(date);
    setShowPicker(false);
    onChange(date);
  }

  return (
    <View>
      <Input
        placeholder={inputPlaceholder}
        onTouchStart={() => setShowPicker(true)}
        value={dateValue? moment(dateValue).format(config.dates.format) : null}
      />
      <DateTimePickerModal
        testID="startDate"
        display="spinner"
        headerTextIOS={headerText}
        date={dateValue}
        mode={mode}
        minimumDate={minimumDate}
        isVisible={showPicker}
        onConfirm={handleDateChange}
        onCancel={() => setShowPicker(false)}
      />
    </View>
  )
}

export {
  InputDateTimePicker,
}
