import React, { useEffect, useLayoutEffect, useState } from 'react';
import {Text, View} from 'react-native';
import { Input } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { HeaderBackButton } from '@react-navigation/stack';
import Animated from 'react-native-reanimated';

import { Card } from 'components/CardComponent';
import {BackendEventService} from "services/api.service";
import {InputDateTimePicker} from "screens/EventCreation/components/InputDateTimePicker";

interface EventInputs {
  title: string;
  description: string;
  start_date: Date;
  end_date: Date;
}

const EventCreation = (): React.ReactElement => {

  const navigation = useNavigation();
  const [eventInputValues, setEventInputValues] = useState({
    title: null,
    description: null,
    start_date: null,
    end_date: null,
  } as EventInputs);
  const [saving, setSaving] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerBackTitle: 'Back',
      headerRight: () => <View pointerEvents={saving ? 'none' : 'auto'}><HeaderBackButton disabled={saving} backImage={() => null} label={'Save'} style={{ paddingRight: 10 }} /></View>
    });
  }, []);


  useEffect(() => {
    setEventInputValues((p: EventInputs): EventInputs => {
      return { ...p, ...{ start_date: startDate } };
    });
  }, [startDate]);

  useEffect(() => {
    setEventInputValues((p: EventInputs): EventInputs => {
      return { ...p, ...{ end_date: endDate } };
    });
  }, [endDate]);

  const handleTitleChange = (title) => {
    setEventInputValues((p: EventInputs): EventInputs => {
      return { ...p, ...{ title: title } };
    });
  }

  const handleDescriptionChange = (d) => {
    setEventInputValues((p: EventInputs): EventInputs => {
      return { ...p, ...{ description: d } };
    });
  }

  const save = async (): Promise<void> => {
    setSaving(true);
    try {
      const r = await BackendEventService.createEvent(eventInputValues);
    } catch (e) {

    }
  }

  return (
    <Animated.ScrollView style={{ backgroundColor: '#ffffff' }}>
      <Card>
        <Input placeholder={'Title'} onChangeText={handleTitleChange} />
      </Card>
      <Card>
        <Input multiline={true} numberOfLines={4} placeholder={'Description'} onChangeText={handleDescriptionChange}/>
      </Card>
      <Card>
        <InputDateTimePicker
          onChange={d => setStartDate(d)}
          headerText={"Start Date"}
          inputPlaceholder={"Select Start Date"}
        />
      </Card>
      <Card>
        <InputDateTimePicker
          minimumDate={startDate}
          onChange={d => setEndDate(d)}
          headerText={"End Date"}
          inputPlaceholder={"Select End Date"}
        />
      </Card>
      <Card>
        <Text>
          { JSON.stringify(eventInputValues)}
        </Text>
      </Card>
    </Animated.ScrollView>
  );
};

export default EventCreation;
