import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { SafeAreaView as BaseSafeAreaView } from 'react-native-safe-area-context';
import { Styles } from 'styles/Styles';

interface Props {
  style?: StyleProp<ViewStyle>;
  children: React.ReactNode;
}

const SafeAreaView = (props: Props): React.ReactElement => {
  return (
    <BaseSafeAreaView style={{ flex: 1, backgroundColor: Styles.colors.white }} {...props}>
      {props.children}
    </BaseSafeAreaView>
  );
};

export {
  SafeAreaView,
};
