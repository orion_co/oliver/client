import React from 'react';
import { StyleSheet, Easing, View, Animated, Modal } from 'react-native';
import { Styles } from '@/styles/Styles';

export interface LoaderProps {
  loading: boolean;
  size?: number;
}

const timing = 800;

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: Styles.colors.white,
  },
  activityIndicatorWrapper: {
    backgroundColor: Styles.colors.white,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export class LoaderComponent extends React.Component<LoaderProps> {

  public static defaultProps: LoaderProps = { loading: true, size: 40 };
  spinValue = new Animated.Value(0);

  public componentDidMount(): void {
    this.spin();
  }

  public spin(): void {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: timing,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => this.spin());
  }

  public render(): React.ReactNode {
    const { size, loading } = this.props;
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });

    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={loading}
      >
        <Animated.View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <Animated.Image
              style={{
                width: size,
                height: size,
                transform: [{ rotate: spin }],
              }}
              source={require('./../../assets/loader.jpg')}
            />
          </View>
        </Animated.View>
      </Modal>
    );
  }

}
