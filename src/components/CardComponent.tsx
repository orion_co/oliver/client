import React from 'react';
import { StyleProp, ViewStyle, View } from 'react-native';
import { Styles } from 'styles/Styles';

interface Props {
  style?: StyleProp<ViewStyle>;
  children: React.ReactNode;
}

const Card = (props: Props): React.ReactElement => {
  return (
    <View style={Styles.card} {...props}>
      {props.children}
    </View>
  );
};

export {
  Card,
};
