
const Styles = {
  card: {
    radius: 5,
    margin: 0,
    padding: 10,
    backgroundColor: 'rgb(255, 255, 255)',
    marginBottom: 0,
    borderRadius: 5,
  },
  colors: {
    grey40: 'rgba(206, 212, 218, .40)',
    grey: 'rgb(206, 212, 218)',
    white: 'rgb(255, 255, 255)',
    prpl: 'rgb(120,36, 186)',
    prpl20: 'rgba(120,36, 186, .20)',
  },
};

export {
  Styles,
};
