process.env.NODE_ENV = process.env.NODE_ENV || 'production';

export default {
  env: process.env.NODE_ENV,

  dates: {
    format: 'MMM DD YYYY, h:mm a',
  },

  log: {
    level: 'silly',
  },
};
