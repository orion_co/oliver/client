import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import RootNavigator from 'navigation/RootNavigator';

const AppStack = createStackNavigator();

const AppNavigator = (): React.ReactElement => {
  return (
    <NavigationContainer>
      <AppStack.Navigator >
        <AppStack.Screen options={{ headerShown: false }} name="AppStack" component={RootNavigator} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
