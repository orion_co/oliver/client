import throttle from 'lodash.throttle';
import routes from './routes';

const n = (navigate: any) => {
  return throttle(navigate, 1000, { trailing: false });
};

const openEventsList = (navigation: any) => (props = {}) => {
  navigation.navigate(routes.EVENTS_LIST, props);
};

const openEventDetail = (navigation: any) => (props = {}) => {
  navigation.navigate(routes.EVENT_DETAILS, props);
};

const openEventCreation = (navigation: any) => (props = {}) => {
  navigation.navigate(routes.EVENT_CREATION, props);
};

const navigate = (navigation: any) => ({
  goBack: navigation.goBack,
  openEventsList: n(openEventsList(navigation)),
  openEventDetail: n(openEventDetail(navigation)),
  openEventCreation: n(openEventCreation(navigation)),
});

export default navigate;
