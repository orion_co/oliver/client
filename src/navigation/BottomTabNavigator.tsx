import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Styles } from 'styles/Styles';
import { Text } from 'react-native';
import EventsNavigator from 'navigation/EventsNavigator';

const BottomTab = createBottomTabNavigator();

interface Props {
  name: string;
  focused: boolean;
}

function TabBarIcon(props: Props): React.ReactElement {
  return (
    // <Ionicons
    //   name={props.name}
    //   size={28}
    //   color={props.focused ? Styles.colors.prpl : Styles.colors.grey40}
    // />
    <Text />
  );
}

const EventOptions = {
  tabBarLabel: 'Events',
  headerShown: true,
  headerTitle: 'Events',
  title: 'Events',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={'md-calendar'} />
  ),
};

const BottomTabNavigator = (): React.ReactElement => {
  const { bottom } = useSafeAreaInsets();
  return (
    <BottomTab.Navigator
      tabBarOptions={{
        activeTintColor: Styles.colors.prpl,
        inactiveTintColor: Styles.colors.grey40,
        style: {
          backgroundColor: Styles.colors.white,
          borderTopWidth: 2,
          borderTopColor: Styles.colors.prpl20,
          paddingBottom: bottom / 2 + 6,
          margin: 0,
        },
      }}
    >
      <BottomTab.Screen
        name='Events'
        component={EventsNavigator}
      />
    </BottomTab.Navigator>
  );
};

export default BottomTabNavigator;
