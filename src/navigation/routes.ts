export default {
  EVENTS_LIST: 'EventsList',
  EVENT_DETAILS: 'EventDetails',
  EVENT_CREATION: 'EventCreation',
};
