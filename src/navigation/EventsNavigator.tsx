import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { EventsScreen } from 'screens/Events';
import routes from '../navigation/routes';
import EventCreation from 'screens/EventCreation/EventCreation';
import EventDetails from 'screens/Events/EventDetails';

const Stack = createStackNavigator();

const EventsNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode={"float"}>
    <Stack.Screen
      name={routes.EVENTS_LIST}
      options={{ headerTitle: 'Events', title: 'Events' }}
      component={EventsScreen}
    />
    <Stack.Screen options={{ title: null, headerBackTitle: 'Back' }} name={routes.EVENT_DETAILS} component={EventDetails} />
    <Stack.Screen options={{ title: null }} name={routes.EVENT_CREATION} component={EventCreation} />
  </Stack.Navigator>
);

export default EventsNavigator;
