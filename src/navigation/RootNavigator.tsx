import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabNavigator from 'navigation/BottomTabNavigator';

const Stack = createStackNavigator();

const RootNavigator = (): React.ReactElement => {
  return (
    <Stack.Navigator mode="modal">
      <Stack.Screen options={{ headerShown: false, title: null }} name="BottomTab" component={BottomTabNavigator} />
    </Stack.Navigator>
  );
};

export default RootNavigator;
