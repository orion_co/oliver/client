module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        "module-resolver",
        {
          root: ["."],
          extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
          alias: {
            '@': "./src",
            services: "./src/services",
            components: "./src/components",
            screens: "./src/screens",
            navigation: "./src/navigation",
            config: "./src/config",
            styles: "./src/styles"
          },
        },
      ],
      'react-native-reanimated/plugin'
    ],
  };
};
