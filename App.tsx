import React, {useState, useEffect} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import * as Font from 'expo-font';
import AppNavigator from "navigation/AppNavigator";
import { Ionicons } from '@expo/vector-icons';
import {SafeAreaView} from "components/SafeAreaView";

const theme = {
  // ...DefaultTheme,
  flex: 1,
  colors: {
    // ...DefaultTheme.colors,
    background: 'red',
    backgroundColor: 'white',
  },
};

export default function App(): React.ReactElement {

  const [isLoadingComplete, setLoadingComplete] = useState(true);

  useEffect(() => {
    Promise.all([
      Font.loadAsync({
        ...Ionicons.font,
      }),
    ])
      .then(() => {
        setLoadingComplete(true);
      })
  }, []);

  return isLoadingComplete && (
    <SafeAreaProvider >
      <AppNavigator/>
    </SafeAreaProvider>
  );
}
